#pragma once
#include <functional>

template<typename T, typename FreeType, FreeType deallocator>
class GeneralGuard
{
public:
	GeneralGuard(T object);
	~GeneralGuard();

	GeneralGuard<T, FreeType, deallocator>& operator=(const GeneralGuard<T, FreeType, deallocator>& other) = delete;
	GeneralGuard(const GeneralGuard<T, FreeType, deallocator>& other) = delete;

	GeneralGuard<T, FreeType, deallocator>& operator=(GeneralGuard<T, FreeType, deallocator>&& other);
	GeneralGuard(GeneralGuard<T, FreeType, deallocator>&& other);

	T get();

private:
	T object;
};

template<typename T, typename FreeType, FreeType deallocator>
inline GeneralGuard<T, FreeType, deallocator>::GeneralGuard(T object) : object(object)
{
}

template<typename T, typename FreeType, FreeType deallocator>
inline GeneralGuard<T, FreeType, deallocator>::~GeneralGuard()
{
	if (object == nullptr) return;
	try {
		deallocator(object);
		object = nullptr;
	} catch(...) {}
}

template<typename T, typename FreeType, FreeType deallocator>
inline GeneralGuard<T, FreeType, deallocator>& GeneralGuard<T, FreeType, deallocator>::operator=(GeneralGuard<T, FreeType, deallocator>&& other)
{
	T tmp = other.object;
	other.object = nullptr;
	object = tmp;

	return *this;
}

template<typename T, typename FreeType, FreeType deallocator>
inline GeneralGuard<T, FreeType, deallocator>::GeneralGuard(GeneralGuard<T, FreeType, deallocator>&& other)
{
	*this = std::move(other);
}

template<typename T, typename FreeType, FreeType deallocator>
inline T GeneralGuard<T, FreeType, deallocator>::get()
{
	return object;
}


#define GUARD_TYPE(obj_type, function) GeneralGuard<obj_type, decltype(function), function>
#define MAKE_GUARD(object, function) (GUARD_TYPE(decltype(object), function)(object))