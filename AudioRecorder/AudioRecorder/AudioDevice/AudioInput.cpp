#include "AudioInput.h"
#include <chrono>
#include <thread>

WAVEFORMATEX create_wave_header(WORD format, DWORD sapmle_rate, DWORD channels, DWORD bytes_per_sample)
{
	WAVEFORMATEX pFormat = { 0 };
	pFormat.wFormatTag = format;
	pFormat.nChannels = channels;                    //  1=mono, 2=stereo
	pFormat.nSamplesPerSec = sapmle_rate;      // 44100
	pFormat.nAvgBytesPerSec = sapmle_rate * channels * bytes_per_sample;   // = nSamplesPerSec * n.Channels *    wBitsPerSample/8
	pFormat.nBlockAlign = channels * bytes_per_sample;                  // = n.Channels * wBitsPerSample/8
	pFormat.wBitsPerSample = bytes_per_sample * 8;              //  16 for high quality, 8 for telephone-grade
	pFormat.cbSize = 0;

	return pFormat;
}

WAVEHDR header_from_buffer(char* buffer, size_t size)
{
	WAVEHDR wave_header = { 0 };
	wave_header.lpData = (LPSTR)buffer;
	wave_header.dwBufferLength = size;

	return wave_header;
}


AudioInput::AudioInput(WORD format, DWORD sample_rate, DWORD challens, DWORD bytes_per_seconds) : format(create_wave_header(
	format,
	sample_rate,
	challens,
	bytes_per_seconds
)), device_guard(nullptr)
{
	waveInOpen(&device, WAVE_MAPPER, &this->format,
		0L, 0L, WAVE_FORMAT_DIRECT);

	device_guard = std::move(MAKE_GUARD(device, waveInClose));
}

std::vector<char> AudioInput::record(size_t seconds)
{
	size_t buffer_size = format.nAvgBytesPerSec * seconds;
	std::vector<char> buffer(buffer_size);
	
	auto wave_header = header_from_buffer(buffer.data(), buffer.size());

	waveInPrepareHeader(device, &wave_header, sizeof(wave_header));
	waveInAddBuffer(device, &wave_header, sizeof(wave_header));
	waveInStart(device);

	while (!(wave_header.dwFlags & WHDR_DONE)) {
		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
	}

	return buffer;
}

AudioOutput::AudioOutput(WORD format, DWORD sample_rate, DWORD challens, DWORD bytes_per_seconds) : format(create_wave_header(
	format,
	sample_rate,
	challens,
	bytes_per_seconds
)), device_guard(nullptr)
{
	waveOutOpen(&device, WAVE_MAPPER, &this->format, 0, 0, WAVE_FORMAT_DIRECT);
	device_guard = std::move(MAKE_GUARD(device, waveOutClose));
}

void AudioOutput::outcast(std::vector<char>& output)
{
	auto wave_header = header_from_buffer(output.data(), output.size());

	waveOutPrepareHeader(device, &wave_header, sizeof(wave_header));
	waveOutWrite(device, &wave_header, sizeof(wave_header)); // Playing the data
	while (!(wave_header.dwFlags & WHDR_DONE)) {
		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
	}
}
