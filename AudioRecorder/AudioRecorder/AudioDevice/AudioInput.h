#pragma once

#include <Windows.h>
#include <vector>
#include "../GeneralGuard/GeneralGuard.h"

#pragma comment(lib, "winmm.lib")

WAVEFORMATEX create_wave_header(WORD format, DWORD sapmle_rate, DWORD channels, DWORD bytes_per_sample);
WAVEHDR header_from_buffer(char* buffer, size_t size);

class AudioInput
{
public:
	AudioInput(WORD format, DWORD sample_rate, DWORD challens, DWORD bytes_per_seconds);
	std::vector<char> record(size_t seconds);

private:
	WAVEFORMATEX format;
	HWAVEIN device;
	GUARD_TYPE(HWAVEIN, waveInClose) device_guard;
};

class AudioOutput
{
public:
	AudioOutput(WORD format, DWORD sample_rate, DWORD challens, DWORD bytes_per_seconds);
	void outcast(std::vector<char>& output);

private:
	WAVEFORMATEX format;
	WAVEHDR header;
	HWAVEOUT device;
	GUARD_TYPE(HWAVEOUT, waveOutClose) device_guard;
};

