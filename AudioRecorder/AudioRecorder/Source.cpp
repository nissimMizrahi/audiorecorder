#include <iostream>
#include <Windows.h>
//#include <Mmreg.h>

#include "AudioDevice/AudioInput.h"
#include "Fourier/Fourier.h"
#include <vector>
#include <future>

#define SAMPLE_RATE 44100
#define CHANNELS 1
#define BYTES_PER_SAMPLE 2

std::vector<std::vector<double>> data2channels(short* buffer, size_t length, size_t channels)
{
	std::vector<std::vector<double>> channels_data(channels);
	for (auto& channel : channels_data) channel.resize(length / channels);


	for (int channel_iter = 0; channel_iter < channels; channel_iter++) {
		for (int i = channel_iter; i < length / channels; i += channels) {
			channels_data[channel_iter][i] = buffer[i];
		}
	}

	return channels_data;
}

void channels2data(short* buffer, size_t length, std::vector<std::vector<double>>& channels_data)
{
	auto channels = channels_data.size();
	for (int channel_iter = 0; channel_iter < channels; channel_iter++) {
		for (int i = channel_iter; i < length / channels; i += channels) {
			buffer[i] = channels_data[channel_iter][i];
		}
	}
}

void fourier(double* channel, size_t size)
{
	std::vector<Signal> signals(size);
	dft(channel, signals.data(), size);

	for (auto& signal : signals) {
		//std::cout << signal.amp() << std::endl;
		if (signal.amp() > 15) {
			signal.kill();
		}
	}

	idft(signals.data(), channel, signals.size());
}

void transform(short* buffer, size_t length, size_t channels)
{
	std::vector<std::vector<double>> channels_data = data2channels(buffer, length, channels);
	
	{
		std::vector<std::future<void>> futures;

		for (auto& channel : channels_data) {
			size_t block_size = 1000;
			for (int i = 0; i < (channel.size() - block_size); i += block_size) {
				auto future = std::async(std::launch::async, fourier, channel.data() + i, block_size);
				futures.emplace_back(std::move(future));
			}
		}
	}

	channels2data(buffer, length, channels_data);
}

int main()
{
	AudioInput mic(WAVE_FORMAT_PCM,
		SAMPLE_RATE,
		CHANNELS,
		BYTES_PER_SAMPLE
	);

	AudioOutput headset(WAVE_FORMAT_PCM,
		SAMPLE_RATE,
		CHANNELS,
		BYTES_PER_SAMPLE
	);
	


	while (true) {
		auto output = mic.record(1);
		std::thread([&headset, output]() {
			auto bla = std::move(output);
			auto data = (short*)bla.data();
			transform(data, bla.size() / sizeof(short), CHANNELS);
			headset.outcast(bla);
		}).detach();
	}
	
	//std::vector<char> buffer;
	//
	//__asm {
	//	//int 3
	//
	//	push SECONDS_RECORDING
	//	lea eax, [buffer]
	//	push eax
	//	call record_seconds
	//	add esp, 0x8
	//}


	return 0;
}