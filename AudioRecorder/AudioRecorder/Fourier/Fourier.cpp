#include "Fourier.h"
#include <math.h>


Signal& Signal::operator*=(double& x)
{
	this->real *= x;
	this->im *= x;

	return *this;
}

Signal& Signal::operator*(double& x)
{
	return Signal(*this) *= x;
}

Signal& Signal::operator*=(const Signal& other)
{
	double t_real = this->real * other.real - this->im * other.im;
	double t_im = this->real * other.im + this->im * other.real;

	this->im = t_im;
	this->real = t_real;

	return *this;
}

Signal& Signal::operator*(const Signal& other)
{
	return Signal(*this) *= other;
}

Signal& Signal::operator+=(const Signal& other)
{
	this->real += other.real;
	this->im += other.im;

	return *this;
}

Signal& Signal::operator+(const Signal& other)
{
	return Signal(*this) += other;
}

void Signal::kill()
{
	this->real = 0.0;
	this->im = 0.0;
}

double Signal::amp()
{
	return sqrt(pow(this->real, 2) + pow(this->im, 2));
}

double Signal::phase()
{
	return atan(this->im / (this->real != 0.0 ? this->real : 1.0));
}

void dft(double* input, Signal* output, long N)
{
	for (long k = 0; k < N; k++)
	{
		output[k] = { 0, 0, 0 };

		for (long n = 0; n < N; n++)
		{
			double num = (TWO_PI * k * n) / N;

			Signal part = {
			  cos(num),
			  -sin(num),
			  0
			};

			part *= input[n];
			output[k] += part;
		}

		double tmp = 1.0 / N;
		output[k] *= tmp;
		output[k].freq = k;
	}
}

void idft(Signal* input, double* output, long N)
{
	for (long n = 0; n < N; n++)
	{
		Signal sum = { 0, 0, 0 };

		for (long k = 0; k < N; k++)
		{
			double num = (TWO_PI * k * n) / N;

			Signal part = {
			  cos(num),
			  sin(num),
			  0
			};

			part *= input[k];
			sum += part;
		}

		output[n] = sum.real;
	}
}

void gpu_idft(Signal* input, double* output, long N)
{
	for (long n = 0; n < N; n++)
	{
		Signal sum = { 0, 0, 0 };

		for (long k = 0; k < N; k++)
		{
			double num = (TWO_PI * k * n) / N;

			Signal part = {
			  cos(num),
			  sin(num),
			  0
			};

			part *= input[k];
			sum += part;
		}

		output[n] = sum.real;
	}
}